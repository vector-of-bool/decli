#include <decli/argparse.hpp>

#include <catch/catch.hpp>


using namespace decli::keyword_args;


TEST_CASE("Basic cli")
{
    decli::cli app{"test-app",
                   action{[]
                          {
                          }}};
    // Invalid argumetns to command
    CHECK(app.execute({"test-app", "bar"}) == 2);
    // Valid arguments: First argument is ignored
    CHECK(app.execute({"test-app"}) == 0);
}


TEST_CASE("One positional")
{
    decli::cli app{"test-app",
                   decli::positional{"foobar"},
                   action{[]
                          {
                          }}};
    // Positional was required
    CHECK(app.execute({"test-app"}) == 2);
    // Too many arguments!
    CHECK(app.execute({"test-app", "bar", "baz"}) == 2);
    // Just right...
    CHECK(app.execute({"test-app", "bar"}) == 0);

    auto args = app.parse_arguments({"test-app", "bar"});
    CHECK(args.arg("foobar") == "bar");
    CHECK_THROWS_AS(args.arg("bar"), std::out_of_range);
}


TEST_CASE("One required and one optional")
{
    decli::cli app{"test-app",
                   decli::positional{"foo"},
                   decli::positional{"bar", required{false}},
                   action{[]
                          {
                          }}};
    // No required positional passed
    CHECK(app.execute({"test-app"}) == 2);
    // One passed: okay
    CHECK(app.execute({"test-app", "bar"}) == 0);
    // Two passed: also okay
    CHECK(app.execute({"test-app", "bar", "baz"}) == 0);
    // Three passed? No go
    CHECK(app.execute({"test-app", "bar", "baz", "qux"}) == 2);

    auto args = app.parse_arguments({"test-app", "bar"});
    CHECK(args.arg("foo") == "bar");
    CHECK_FALSE(args.maybe_arg("bar"));
    CHECK(args.arg_or("bar", "cat") == "cat");
    CHECK(args.arg_or("foo", "dog") == "bar");
    CHECK_THROWS_AS(args.arg("bar"), std::out_of_range);
}


TEST_CASE("Help")
{
    decli::cli app{"test-app"};
    CHECK_THROWS_AS(app.parse_arguments({"test-app", "--help"}), decli::help_requested);
    CHECK_THROWS_AS(app.parse_arguments({"test-app", "-h"}), decli::help_requested);
}


TEST_CASE("Parameters")
{
    decli::cli app{"test-app", decli::param{"--what", shorter{'W'}}};

    SECTION("Value equals flag")
    {
        auto args = app.parse_arguments({"test-app", "--what=foo"});
        CHECK(args.arg("--what") == "foo");
    }

    SECTION("Value follows flag")
    {
        auto args = app.parse_arguments({"test-app", "--what", "bar"});
        CHECK(args.arg("--what") == "bar");
    }

    SECTION("Value joined to short")
    {
        auto args = app.parse_arguments({"test-app", "-Wbaz"});
        CHECK(args.arg("--what") == "baz");
    }

    SECTION("Value follows short")
    {
        auto args = app.parse_arguments({"test-app", "-W", "qux"});
        CHECK(args.arg("--what") == "qux");
    }

    SECTION("Argument not provided")
    {
        auto args = app.parse_arguments({"test-app"});
        CHECK_THROWS_AS(args.arg("--what"), std::out_of_range);
    }

    SECTION("Flag value missing")
    {
        CHECK_THROWS_AS(app.parse_arguments({"test-app", "--what"}),
                        decli::invalid_arguments);
    }

    SECTION("Short value missing")
    {
        CHECK_THROWS_AS(app.parse_arguments({"test-app", "-W"}), decli::invalid_arguments);
    }

    SECTION("Empty equals value")
    {
        auto args = app.parse_arguments({"test-app", "--what="});
        CHECK(args.arg("--what") == "");
    }

    SECTION("Flag with key")
    {
        app = decli::cli{"test-app", decli::param{"--foo", key{"eggs"}}};
        auto args = app.parse_arguments({"test-app", "--foo=bar"});
        CHECK(args["eggs"] == "bar");
        // Keys replace the flag in the arguments
        CHECK_FALSE(args.maybe_arg("--foo"));
    }
}


TEST_CASE("Multi")
{
    decli::cli
        app{"test-app",
            decli::multi{"--include",
                         shorter{'I'},
                         key{"include-dirs"},
                         summary{
                             "Add an include directory to the search path"}}};

    CHECK_NOTHROW(app.parse_arguments({"test-app", "--include=one", "--include=two"}));
    CHECK_NOTHROW(app.parse_arguments({"test-app", "-I", "one", "-Itwo"}));

    decli::string_list cmd{ "test-app" };
    auto args = app.parse_arguments(cmd);
    CHECK(args.multi("include-dirs").size() == 0);

    cmd.emplace_back("--include");
    cmd.emplace_back("foo");
    CHECK(app.parse_arguments(cmd).multi("include-dirs").size() == 1);

    cmd.emplace_back("--include=bar");
    CHECK(app.parse_arguments(cmd).multi("include-dirs").size() == 2);

    cmd.emplace_back("-Ifoobar");
    CHECK(app.parse_arguments(cmd).multi("include-dirs").size() == 3);

    CHECK(app.parse_arguments(cmd).multi("include-dirs") == (decli::string_list{"foo", "bar", "foobar"}));
}
