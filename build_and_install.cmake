find_program(NMAKE_EXECUTABLE nmake)
find_program(NINJA_EXECUTABLE ninja)
find_program(MAKE_EXECUTABLE make)

if(NINJA_EXECUTABLE)
    set(generator -G Ninja)
elseif(NMAKE_EXECUTABLE)
    set(generator -G "NMake Makefiles")
elseif(MAKE_EXECUTABLE)
    set(generator -G "Unix Makefiles")
endif()

set(bindir "${CMAKE_CURRENT_LIST_DIR}/build")
execute_process(
    COMMAND "${CMAKE_COMMAND}"
        "-H${CMAKE_CURRENT_LIST_DIR}"
        "-B${bindir}"
        -DCMAKE_BUILD_TYPE=Release
        ${generator}
    RESULT_VARIABLE res
    )

if(res)
    message(FATAL_ERROR "Failed to configure decli [${res}]")
endif()

execute_process(
    COMMAND "${CMAKE_COMMAND}"
        --build "${bindir}"
    RESULT_VARIABLE res
    )

if(res)
    message(FATAL_ERROR "Failed to build decli [${res}]")
endif()

execute_process(
    COMMAND ctest
    WORKING_DIRECTORY "${bindir}"
    RESULT_VARIABLE res
    )

if(res)
    message(FATAL_ERROR "Tests failed [${res}]")
endif()

execute_process(
    COMMAND "${CMAKE_COMMAND}" -P "${bindir}/cmake_install.cmake"
    RESULT_VARIABLE res
    )

if(res)
    message(FATAL_ERROR "Install failed [${res}]")
endif()

message(STATUS "Done! Now use find_package(decli) in your CMake project to use it")
