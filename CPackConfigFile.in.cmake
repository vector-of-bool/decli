# General CPack settings
string(TOLOWER "@PROJECT_NAME@" CPACK_PACKAGE_NAME)
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Create declarative command lines using standard C++")
set(CPACK_PACKAGE_CONTACT "vector-of-bool <vectorofbool+decli@gmail.com>")

set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}
 DeCLI is a library for easily creating command line interfaces in C++
 .
 Parsing command lines can be difficult, and ugly. DeCLI makes this easy, and
 even generates very nice help text from the command line specification.
 .
 DeCLI requires only a C++ compiler to get started, and uses CMake to make
 configuration a sinch to use.")

find_program(LSB_RELEASE lsb_release)

function(lsb_command out arg)
    execute_process(
        COMMAND ${LSB_RELEASE} -${arg}s
        OUTPUT_VARIABLE value
        OUTPUT_STRIP_TRAILING_WHITESPACE
        )
    set(${out} ${value} PARENT_SCOPE)
endfunction()

if("@CMAKE_SIZEOF_VOID_P@" STREQUAL 8)
    set(arch x86_64)
    set(deb_arch amd64)
    set(rpm_arch x86_64)
else()
    set(arch x86)
    set(deb_arch i386)
    set(rpm_arch i386)
endif()

set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE ${deb_arch})
set(CPACK_RPM_PACKAGE_ARCHITECTURE ${rpm_arch})

set(url "https://bitbucket.org/vector-of-bool/decli")
set(CPACK_DEBIAN_PACKAGE_HOMEPAGE ${url})
set(CPACK_RPM_PACKAGE_URL ${url})

if(CMAKE_HOST_SYSTEM_NAME STREQUAL Linux AND NOT LSB_RELEASE AND CPACK_GENERATOR MATCHES RPM|DEB)
    message(FATAL_ERROR "Install lsb_release for creating Debian/RedHat packages")
endif()

# A reasonable default.
set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}-${CMAKE_HOST_SYSTEM_NAME}-${arch})

if(CPACK_GENERATOR STREQUAL DEB)
    lsb_command(LSB_ID i)
    lsb_command(LSB_VERSION r)
    string(TOLOWER ${LSB_ID} dist_id)
    set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}_${CPACK_PACKAGE_VERSION}-0${dist_id}${LSB_VERSION}_${CPACK_DEBIAN_PACKAGE_ARCHITECTURE})
elseif(CPACK_GENERATOR STREQUAL RPM)
    set(CPACK_PACKAGE_FILE_NAME ${CPACK_PACKAGE_NAME}-${CPACK_PACKAGE_VERSION}-1.${CPACK_RPM_PACKAGE_ARCHITECTURE})
endif()

