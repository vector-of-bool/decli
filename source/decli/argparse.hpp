#ifndef DECLI_ARGPARSE_HPP_INCLUDED
#define DECLI_ARGPARSE_HPP_INCLUDED

#include <string>
#include <vector>
#include <ostream>
#include <cassert>
#include <stdexcept>
#include <map>
#include <functional>


namespace decli
{

inline namespace argparse
{

inline namespace types
{

using string = std::string;
template <typename T> using vector = std::vector<T>;

using string_list = vector<string>;
using string_map = std::map<string, string>;
using bool_map = std::map<string, bool>;

class cli;
class arguments;


class action_fn
{
    std::function<int(const arguments&)> _fn;

public:
    action_fn() = default;

    // clang-format off
    template <
        typename Func,
        typename = typename std::enable_if<
            std::is_same<
                int,
                typename std::result_of<Func(const arguments&)>::type
            >::value
        >::type
    >
    // clang-format on
    action_fn(Func&& fn)
        : _fn{std::forward<Func>(fn)}
    {
    }

    // clang-format off
    template<
        typename Func,
        typename = typename std::enable_if<
            std::is_same<
                void,
                typename std::result_of<Func(const arguments&)>::type
            >::value
        >::type,
        typename = void
    >
    // clang-format on
    action_fn(Func&& fn)
        : _fn{[fn](const arguments& args)
              {
                  fn(args);
                  return 0;
              }}
    {
    }

    // clang-format off
    template<
        typename Func,
        typename = typename std::enable_if<
            std::is_same<
                int,
                typename std::result_of<Func()>::type
            >::value
        >::type,
        typename = void,
        typename = void
    >
    // clang-format on
    action_fn(Func&& fn)
        : _fn{[fn](const arguments&)
              {
                  return fn();
              }}
    {
    }

    // clang-format off
    template<
        typename Func,
        typename = typename std::enable_if<
            std::is_same<
                void,
                typename std::result_of<Func()>::type
            >::value
        >::type,
        typename = void,
        typename = void,
        typename = void
    >
    // clang-format on
    action_fn(Func&& fn)
        : _fn{[fn](const arguments&)
              {
                  fn();
                  return 0;
              }}
    {
    }

    int operator()(const arguments& args) const { return _fn(args); }

    explicit operator bool() const { return static_cast<bool>(_fn); }
};


#define OPT_LIST                                                               \
    X(string, summary)                                                         \
    X(string, version)                                                         \
    X(char, shorter)                                                           \
    X(string, key)                                                             \
    X(string, help)                                                            \
    X(bool, required)                                                          \
    X(string, collect_unparsed)                                                \
    X(action_fn, action)

#define X(type, name)                                                          \
    struct name                                                                \
    {                                                                          \
        type value;                                                            \
        name(type t)                                                           \
            : value{std::move(t)}                                              \
        {                                                                      \
        }                                                                      \
    };
inline namespace keyword_args
{
OPT_LIST
} /* keyword_args */
#undef X


class arguments
{
    string_map _valued_args;
    bool_map _switches;
    std::reference_wrapper<const cli> _subcommand;
    vector<string> _unparsed;
    std::map<string, string_list> _multis;

public:
    arguments(string_map args,
              bool_map switches,
              std::reference_wrapper<const cli> subc,
              vector<string> unparsed,
              std::map<string, string_list> multis)
        : _valued_args{move(args)}
        , _switches{move(switches)}
        , _subcommand{subc}
        , _unparsed{move(unparsed)}
        , _multis{move(multis)}
    {
    }

    const string_map& args_map() const { return _valued_args; }
    const bool_map& toggles() const { return _switches; }
    const cli& subcommand() const { return _subcommand; }

    const string& arg(const string& key) const
    {
        auto argptr = maybe_arg(key);
        if (!argptr)
            throw std::out_of_range(
                "No such argument with given key was provided by the user: "
                + key);
        return *argptr;
    }

    const string* maybe_arg(const string& key) const
    {
        auto iter = _valued_args.find(key);
        if (iter != end(_valued_args)) return &iter->second;
        return nullptr;
    }

    string arg_or(const string& key, string default_value) const
    {
        auto ptr = maybe_arg(key);
        if (!ptr) return default_value;
        return *ptr;
    }

    const string& operator[](const string& key) const { return arg(key); }

    bool given(const string& key) const
    {
        auto iter = _switches.find(key);
        if (iter == end(_switches))
            throw std::out_of_range("Switch " + key
                                    + " is not a known program switch");
        return iter->second;
    }

    const string_list& unparsed() const { return _unparsed; }
    const std::map<string, string_list>& multis() const { return _multis; }
    const string_list& multi(const string& key) const
    {
        auto iter = _multis.find(key);
        if (iter == end(_multis)) throw std::out_of_range{"No such multi-parameter: " + key};
        return iter->second;
    }

    void apply(const arguments& other)
    {
        for (auto& kv : other.args_map()) _valued_args[kv.first] = kv.second;
        for (auto& sw : other.toggles()) _switches[sw.first] = sw.second;
        _subcommand = other.subcommand();
        _unparsed.insert(end(_unparsed),
                         begin(other.unparsed()),
                         end(other.unparsed()));
        _multis.insert(begin(other.multis()), end(other.multis()));
    }
};


class param
{
    string _flagstr;
    string _key;
    string _summary;
    string _help;
    char _shorter = '\0';

#define DECLI_APPLY_NAMED_ARGS_MAGIC                                             \
    void _apply_args() {}                                                      \
                                                                               \
    template <typename T, typename... Args>                                    \
    void _apply_args(T&& t, Args&&... args)                                    \
    {                                                                          \
        _apply_one_arg(std::forward<T>(t));                                    \
        _apply_args(std::forward<Args>(args)...);                              \
    }                                                                          \
                                                                               \
    template <typename T> void _apply_one_arg(T)                               \
    {                                                                          \
        static_assert(                                                         \
            std::is_same<T, void>::value,                                      \
            "The named argument given is not applicable in this context");     \
    }

#define DECLI_NAMED_ARGS_PARAM_COMMON                                            \
    void _apply_one_arg(struct shorter s) { _shorter = s.value; }              \
    void _apply_one_arg(struct summary s) { _summary = s.value; }              \
    void _apply_one_arg(struct help s) { _help = s.value; }

    DECLI_APPLY_NAMED_ARGS_MAGIC
    DECLI_NAMED_ARGS_PARAM_COMMON

    void _apply_one_arg(struct key k) { _key = k.value; }

public:
    template <typename... Args>
    param(string flagstr, Args&&... args)
        : _flagstr{std::move(flagstr)}
    {
        _apply_args(std::forward<Args>(args)...);
    }

    const string& longer() const { return _flagstr; }
    const string& key() const { return _key; }
    const string& summary() const { return _summary; }
    const string& help() const { return _help; }
    char shorter() const { return _shorter; }
};

class toggle
{
    string _flagstr;
    char _shorter = '\0';
    string _summary;
    string _help;

    DECLI_APPLY_NAMED_ARGS_MAGIC
    DECLI_NAMED_ARGS_PARAM_COMMON

public:
    template <typename... Args>
    toggle(string flagstr, Args&&... args)
        : _flagstr{std::move(flagstr)}
    {
        _apply_args(std::forward<Args>(args)...);
    }

    const string& longer() const { return _flagstr; }
    const string& summary() const { return _summary; }
    const string& help() const { return _help; }
    char shorter() const { return _shorter; }
};


class positional
{
    string _name;
    bool _required = true;

    DECLI_APPLY_NAMED_ARGS_MAGIC

    void _apply_one_arg(struct required r) { _required = r.value; }

public:
    template <typename... Args>
    explicit positional(string name, Args&&... args)
        : _name{move(name)}
    {
        _apply_args(std::forward<Args>(args)...);
    }

    const string& name() const { return _name; }

    bool required() const { return _required; }
};


class multi
{
    string _name;
    string _summary;
    string _help;
    char _shorter = '\0';
    string _key;
    DECLI_APPLY_NAMED_ARGS_MAGIC
    DECLI_NAMED_ARGS_PARAM_COMMON

    void _apply_one_arg(struct key k) { _key = k.value; }

public:
    template <typename... Args>
    explicit multi(string name, Args&&... args)
        : _name{move(name)}
    {
        _apply_args(std::forward<Args>(args)...);
    }

    const string& longer() const { return _name; }
    const string& key() const { return _key; }
    char shorter() const { return _shorter; }
    const string& summary() const { return _summary; }
    const string& help() const { return _help; }
};


class invalid_arguments : std::invalid_argument
{
public:
    invalid_arguments()
        : std::invalid_argument{""}
    {
    }
};


class help_requested : std::exception
{
};


class cli
{
    string _name;
    string _summary;
    string _collect_unparsed;
#ifdef CMAKE_PROJECT_VERSION
    string _version = CMAKE_PROJECT_VERSION;
#else
    string _version;
#endif

    vector<param> _parameters;
    vector<toggle> _switches;
    vector<positional> _required_positionals;
    vector<positional> _optional_positionals;
    vector<multi> _multis;
    vector<cli> _subcommands;
    action_fn _action;

    DECLI_APPLY_NAMED_ARGS_MAGIC

    void _apply_one_arg(summary s) { _summary = s.value; }

    void _apply_one_arg(param new_param)
    {
        _parameters.emplace_back(std::move(new_param));
    }

    void _apply_one_arg(toggle toggle)
    {
        _switches.emplace_back(std::move(toggle));
    }

    void _apply_one_arg(version v) { _version = v.value; }

    void _apply_one_arg(cli s) { _subcommands.emplace_back(std::move(s)); }

    void _apply_one_arg(action a)
    {
        assert(!_action && "More than one action given to a command");
        _action = std::move(a.value);
    }

    void _apply_one_arg(positional pos)
    {
        if (pos.required())
            _required_positionals.emplace_back(std::move(pos));
        else
            _optional_positionals.emplace_back(std::move(pos));
    }

    void _apply_one_arg(multi m)
    {
        _multis.emplace_back(std::move(m));
    }

    void _apply_one_arg(collect_unparsed c) { _collect_unparsed = c.value; }

public:
    using argiter = string_list::const_iterator;

    template <typename... Args>
    explicit cli(const string& name, Args&&... args)
        : _name{name}
    {
        _apply_args(std::forward<Args>(args)...);
    }

    int execute(int argc, char** argv)
    {
        return execute(string_list{argv, argv + argc});
    }

    int execute(const string_list& args)
    {
        return execute(begin(args), end(args));
    }

    bool maybe_help(const string& name, argiter first, argiter last) const;
    bool maybe_help(argiter first, argiter last) const
    {
        return maybe_help(_name, first, last);
    }
    bool maybe_version(argiter first, argiter last) const;

    void print_help(const string& name, std::ostream&) const;
    void print_version(std::ostream&) const;

    arguments parse_arguments(const string_list& args) const
    {
        return parse_arguments(*begin(args), begin(args), end(args));
    }
    arguments parse_arguments(string, argiter, argiter) const;

    int execute(argiter first, argiter last) const
    {
        assert(first != last);

        try
        {
            auto args = parse_arguments(*first, first, last);
            return args.subcommand().execute_with(args);
        }
        catch (invalid_arguments e)
        {
            return 2;
        }
        catch( help_requested )
        {
            return 0;
        }
    }

    const string& name() const { return _name; }

    int execute_with(const arguments& args) const
    {
        return _action(args);
    }
};

} /* types */

} /* argparse */

} /* decli */

#endif  // DECLI_ARGPARSE_HPP_INCLUDED
