#include "argparse.hpp"

#include <algorithm>

#include <iostream>
#include <iomanip>
#include <tuple>


using namespace decli::argparse::types;
using argiter = cli::argiter;


bool cli::maybe_help(const string& name, argiter first, argiter last) const
{
    if (std::find(first, last, string{"--help"}) != last
        || std::find(first, last, "-h") != last)
    {
        print_help(name, std::cout);
        return true;
    }
    return false;
}


bool cli::maybe_version(argiter first, argiter last) const
{
    if (std::find(first, last, "--version") != last
        || std::find(first, last, "-v") != last)
    {
        print_version(std::cout);
        return true;
    }
    return false;
}


void cli::print_help(const string& name, std::ostream& out) const
{
    out << "Usage:\n"
        << "  " << name;
    out << ' ';

    unsigned cur_col = 3 + name.length();
    const unsigned max_col = 60;

    unsigned indent = 3 + name.length();
    enum show_space
    {
        yes_space,
        no_space
    };
    auto newline = [&out, &indent, &cur_col]
    {
        out << '\n';
        for (auto i = indent; i--;) out << ' ';
        cur_col = indent;
    };
    auto write_word
        = [&out, &indent, &cur_col, &newline](const string& str, show_space s)
    {
        if (str.length() + cur_col > max_col && indent != cur_col) newline();
        out << str;
        cur_col += str.length();
        if (s == no_space) return;
        out << ' ';
        cur_col++;
    };
    auto write_many_words
        = [&out, &indent, &cur_col, &write_word, &newline](const string& str)
    {
        string word;
        auto last = end(str);
        for (auto iter = begin(str); iter != last; ++iter)
        {
            auto c = *iter;
            word.push_back(c);
            if (c == '\n')
                newline();
            else if (c == '\r')
            { /*noop*/
            }
            else if (std::isspace(c) || iter + 1 == last)
            {
                write_word(word, no_space);
                word.clear();
            }
        }
    };

    string short_chars;
    for (auto& param : _parameters)
        if (param.shorter()) short_chars.push_back(param.shorter());
    for (auto& sw : _switches)
        if (sw.shorter()) short_chars.push_back(sw.shorter());
    for (auto& m : _multis)
        if (m.shorter()) short_chars.push_back(m.shorter());
    if (!short_chars.empty())
    {
        write_word("[-" + short_chars + "]", yes_space);
    }

    for (auto& sw : _switches)
    {
        write_word("[" + sw.longer() + "]", yes_space);
    }

    for (auto& param : _parameters)
    {
        write_word("[" + param.longer() + "=<"
                       + (param.key().empty() ? "?" : param.key())
                       + ">]",
                   yes_space);
    }

    for (auto& multi : _multis)
    {
        write_word("[" + multi.longer() + "=<"
                       + (multi.key().empty() ? "..." : multi.key())
                       + "> ...]",
                   yes_space);
    }

    for (auto& pos : _required_positionals)
    {
        write_word("<" + pos.name() + ">", yes_space);
    }

    for (auto& pos : _optional_positionals)
    {
        write_word("[" + pos.name() + "]", yes_space);
    }

    if (!_collect_unparsed.empty())
        write_word("[" + _collect_unparsed + " [" + _collect_unparsed
                       + " [...]]]",
                   yes_space);

    if (!_subcommands.empty())
    {
        write_word("{", no_space);

        for (auto iter = begin(_subcommands); iter != end(_subcommands); ++iter)
        {
            if (iter + 1 != end(_subcommands))
                write_word(iter->name() + ",", no_space);
            else
                write_word(iter->name(), no_space);
        }
        write_word("}", no_space);
    }

    indent = 2;
    if (!_summary.empty())
    {
        newline();
        newline();
        write_many_words(_summary);
    }

    out << "\n\nOptions:\n";
    out << "  --help \n  -h\n"
        << "      Print this help text and exit\n\n"
        << "  --version \n  -v\n"
        << "      Print the program version and exit\n";

    indent = 6;

    for (auto& toggle : _switches)
    {
        if (toggle.help().empty() && toggle.summary().empty()) continue;
        indent = 2;
        newline();
        out << toggle.longer();
        if (toggle.shorter())
        {
            newline();
            out << "-" << toggle.shorter();
        }
        indent = 6;
        if (!toggle.summary().empty())
        {
            newline();
            write_many_words(toggle.summary());
            if (!toggle.help().empty()) newline();
        }
        if (!toggle.help().empty())
        {
            newline();
            write_many_words(toggle.help());
            newline();
        }
    }

    auto print_param = [&](auto& param)
    {
        // Parameters without summaries or help text get
        // ignored
        if (param.help().empty() && param.summary().empty()) return;
        indent = 2;
        newline();
        out << param.longer() << "=<";
        if (!param.key().empty())
            out << param.key();
        else
            out << "?";
        out << ">";
        if (param.shorter())
        {
            newline();
            out << "-" << param.shorter() << " <";
            if (!param.key().empty())
                out << param.key();
            else
                out << '?';
            out << '>';
        }
        indent = 6;
        if (!param.summary().empty())
        {
            newline();
            write_many_words(param.summary());
            if (!param.help().empty()) newline();
        }
        if (!param.help().empty())
        {
            newline();
            write_many_words(param.help());
            newline();
        }
    };

    if (!_parameters.empty()) out << '\n';
    for (auto& param : _parameters) print_param(param);
    if (!_multis.empty()) out << '\n';
    for (auto& param : _multis) print_param(param);

    if (!_subcommands.empty())
    {
        out << '\n';
        out << "Subcommands:";
        for (auto& c : _subcommands)
        {
            indent = 2;
            newline();
            out << std::left << std::setw(11) << c.name();
            if (!c._summary.empty())
            {
                indent = 2 + 11 + 3;
                cur_col += indent;
                out << " - ";
                write_many_words(c._summary);
            }
        }
    }
    out << '\n';
}


void cli::print_version(std::ostream& out) const
{
    out << _name << " " << _version << '\n';
}


arguments
cli::parse_arguments(string program_name, argiter iter, argiter last) const
{
    string_map args;
    std::map<string, string_list> multi_values;
    for (auto& m : _multis) multi_values[m.key().empty() ? m.longer() : m.key()] = {};
    bool_map switches;
    for (auto& sw : _switches)
    {
        switches.emplace(sw.longer(), false);
    }
    vector<positional> pending_positionals{begin(_required_positionals),
                                           end(_required_positionals)};
    pending_positionals.insert(end(pending_positionals),
                               begin(_optional_positionals),
                               end(_optional_positionals));
    string_map positionals;
    vector<string> unparsed;

    auto set_or_replace_arg
        = [&args](const string& flag, const string& key, const string& value)
    {
        auto& chosen_key = key.empty() ? flag : key;
        auto arg_iter = args.find(chosen_key);
        if (arg_iter == end(args))
            args.emplace_hint(arg_iter, chosen_key, value);
        else
            arg_iter->second = value;
    };
    auto append_multi = [&multi_values](const string& flag, const string& key, const string& value)
    {
        auto& chosen_key = key.empty() ? flag : key;
        auto arg_iter = multi_values.find(chosen_key);
        assert(arg_iter != end(multi_values));
        arg_iter->second.emplace_back(value);
    };
    auto check_positionals_satisfied
        = [this, &pending_positionals, &program_name]
    {
        // We've parsed all the arguments! Check that we
        // have no remaining
        // unsatisfied positional arguments
        if (!pending_positionals.empty()
            && pending_positionals.front().required())
        {
            print_help(program_name, std::cerr);
            std::cerr << "Error: Expected a value for "
                         "argument \"<"
                      << pending_positionals.front().name() << ">\"\n";
            throw invalid_arguments{};
        }
    };

    // Skip over the program name:
    ++iter;
    while (iter != last)
    {
        auto& arg = *iter;
        if (arg == "--help" || arg == "-h")
        {
            print_help(program_name, std::cout);
            throw help_requested{};
        }

        // Check for simple switches
        for (auto& sw : _switches)
        {
            if (sw.longer() == arg)
            {
                switches[sw.longer()] = true;
                goto arg_done;
            }
        }

#define TRY_PARSE_LONG(param, handler)                                         \
    do                                                                         \
    {                                                                          \
        if (param.longer() == arg)                                             \
        {                                                                      \
            /* For command lines like foo --bar baz */                         \
            ++iter;                                                            \
            if (iter == last)                                                  \
            {                                                                  \
                print_help(program_name, std::cerr);                           \
                std::cerr << "Error: Flag \"" << param.longer()                \
                          << "\" expects an argument.\n";                      \
                throw invalid_arguments{};                                     \
            }                                                                  \
            handler(param.longer(), param.key(), *iter);                       \
            goto arg_done;                                                     \
        }                                                                      \
        else if (arg.find(param.longer()) != arg.npos                          \
                 && arg.size() > param.longer().size()                         \
                 && arg[param.longer().size()] == '=')                         \
        {                                                                      \
            /* For command lines like foo --bar=baz */                         \
            auto val = arg.substr(param.longer().size() + 1);                  \
            handler(param.longer(), param.key(), val);                         \
            goto arg_done;                                                     \
        }                                                                      \
    } while (0)

        // Check for a flag with an argument
        for (auto& param : _parameters)
            TRY_PARSE_LONG(param, set_or_replace_arg);

        // Multi-params are params too
        for (auto& multi : _multis)
            TRY_PARSE_LONG(multi, append_multi);

        // Check if we have a shorthand switch
        if (arg.size() > 1 && arg[0] == '-' && arg[1] != '-')
        {
            // Reach here for short flags that use
            // a single hyphen
            auto char_iter = begin(arg) + 1;  // +1 for the leading hyphen
            while (char_iter != end(arg))
            {
                auto c = *char_iter++;
                bool char_ok = false;
                // Check for switches that don't take an argument
                for (auto& sw : _switches)
                {
                    if (sw.shorter() && sw.shorter() == c)
                    {
                        switches[sw.longer()] = true;
                        char_ok = true;
                        break;
                    }
                }

#define TRY_PARSE_SHORT(param, handler)                                        \
    do                                                                         \
    {                                                                          \
        if (param.shorter() && param.shorter() == c)                           \
        {                                                                      \
            if (char_iter != end(arg))                                         \
            {                                                                  \
                /* Reach here with command lines like: foo -oArgumentValue */  \
                string acc;                                                    \
                /* The rest of the shorthand string will be consumed into the  \
                 * argument value */                                           \
                while (char_iter != end(arg)) acc.push_back(*char_iter++);     \
                handler(param.longer(), param.key(), acc);                     \
            }                                                                  \
            else                                                               \
            {                                                                  \
                /* Reach here with command lines like: foo -o ArgumentValue    \
                 * Shift to the next argument */                               \
                iter++;                                                        \
                if (iter == last)                                              \
                {                                                              \
                    /* We reached the command end, but found nothing here.     \
                     * That's no good*/                                        \
                    print_help(program_name, std::cerr);                       \
                    std::cerr << "Error: Short flag \"-" << c                  \
                              << "\" expects an argument\n";                   \
                    throw invalid_arguments{};                                 \
                }                                                              \
                handler(param.longer(), param.key(), *iter);                   \
            }                                                                  \
            char_ok = true;                                                    \
            break;                                                             \
        }                                                                      \
    } while (0)
                for (auto& param : _parameters) TRY_PARSE_SHORT(param, set_or_replace_arg);
                for (auto& multi : _multis) TRY_PARSE_SHORT(multi, append_multi);

                if (!char_ok)
                {
                    // We failed to parse the single-character flag
                    print_help(program_name, std::cerr);
                    std::cerr << "Error: Unknown option \"-" << c << "\"\n";
                    throw invalid_arguments{};
                }
            }
            goto arg_done;
        }

        // Check for a subcommand. This short-circuits
        // everything...
        if (pending_positionals.empty())
        {
            for (auto& cmd : _subcommands)
            {
                if (cmd.name() == arg)
                {
                    check_positionals_satisfied();
                    auto sub_args
                        = cmd.parse_arguments(program_name + " " + cmd.name(),
                                              iter,
                                              last);
                    arguments ret_args{args, switches, *this, unparsed, multi_values};
                    ret_args.apply(sub_args);
                    return ret_args;
                }
            }
        }

        // Just consume the unparsed result as a positional
        if (!pending_positionals.empty())
        {
            set_or_replace_arg(pending_positionals.front().name(), "", *iter);
            pending_positionals.erase(begin(pending_positionals));
            goto arg_done;
        }

        if (!_collect_unparsed.empty())
        {
            unparsed.emplace_back(*iter);
            goto arg_done;
        }

        // If we didn't parse that arg we exit immediately
        print_help(program_name, std::cerr);
        std::cerr << "Error: Unknown argument or subcommand \"" << arg
                  << "\"\n";
        throw invalid_arguments{};

    arg_done:
        ++iter;
    }

    check_positionals_satisfied();
    return {args, switches, *this, unparsed, multi_values};
}
