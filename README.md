
# DeCLI - Declarative Command Line Interfaces in C++
Decli is a small zero-dependency C++ library for creating command line
interfaces to programs. Usage is meant to be as easy as possible.

## Obtaining
### Downloads
Linux packages are available in the
[downloads section](https://bitbucket.org/vector-of-bool/decli/downloads) on
the official BitBucket repository. Windows is fully supported, but installers
are not yet available at this time (sorry Windows users. You can continue
reading to find out how to install and use. Don't worry, it's easy!).

### Install from Source
#### Getting the source
Getting the source is as simple as downloading a source tarball or cloning the
repository. For stable usage, be sure to checkout a version tag or the
``master`` branch. The ``develop`` branch should be used for development of
decli.

    git clone git@bitbucket.org:vector-of-bool/decli.git

The only requirement is that you have CMake and a C++ compiler.

#### Easy-Building
To get started quickly, a single command is sufficient to both build and install:

    cmake -P build_and_install.cmake

You may need to run with elevated privileges for the install step to succeed.

#### More Complicated Building
Building can be done by manually invoking the required CMake commands. This
lets one set more build and install options than the above command:

```bash
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
ctest
cmake -P cmake_install.cmake  # Maybe with a touch of ``sudo``
```

### Creating Packages
Creating system-specific installable packages is fully supported with CPack.
Just set the generator for your system when you invoke CPack.

First you'll need to build and configure the project, then run ``cpack`` from
within the build directory, providing your desired generator on the command line.

For example:
```bash
# For Debian-based systems
cpack -G DEB

# For RedHat systems
cpack -G RPM

# For Windows using the WiX toolset
cpack -G WIX
```

## Include in Your Project
The only officially supported way to use DeCLI is with CMake. As long as you
installed DeCLI to a normal (default) system location, DeCLI can be imported
to a CMake project with only one line:

```cmake
find_package(decli)
```

It is recommended to request a specific version for compatibility.

Linking with DeCLI is simple as well:

```cmake
add_executable(foo
    bar.cpp
    baz.cpp
    qux.cpp
    )

target_link_libraries(foo PRIVATE decli::argparse)
```

This will add the required include directories and link options required for
using DeCLI with the ``foo`` executable. For more usage, see ``HOWTO.md``
