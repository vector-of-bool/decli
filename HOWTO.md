
# How to Make a Command Line Program with DeCLI

Assuming you've followed to steps to get started, this page will show you how to use decli to create a command-line interface. There's only one header to include:

```cpp
#include <decli/argparse.hpp>
```

## Defining our Command Line
The most basic type is ``decli::cli``. Decli is based on keyword-style arguments, so it's easiest to import the namespace containing these arguments to avoid unnecessary typing. You can do this at function scope or global scope.

```cpp
int main(int argc, char** argv)
{
	using namespace decli::keyword_args;
	// ...
}
```

The constructor to ``decli::cli`` takes an arbitrary number of nested parameters that define the command line interface. The first parameters must always be the name of the program. This is used in help text if the command cannot itself be deduced:

```cpp
decli::cli app{
	"my-program",
	// ...
};
```

The only required parameter is ``action``, which is a keyword parameter that defines the action to take when the program is executed. It simply takes a function as it's only constructor argument should be a function to execute, such as a lambda, functor, or function pointer:

```cpp
decli::cli app{
	"my-program",
	action{[](decli::arguments) -> int {
		return 0;
	}}
};
```

The function may only return ``void`` or ``int``. If it returns ``void``, the program will always return ``0``. If it returns ``int``, the return value will be the return code of the program.

## Execute the Program
The most useful way to execute the command line program is to use the ``execute`` member of ``decli::cli``:

```cpp
return app.execute(argc, argv);
```

This will parse the arguments, execute our action, and return the return code. All together, here is our example program:

```cpp
#include <decli/argparse.hpp>

int main(int argc, char** argv)
{
	using namespace decli::keyword_args;

	decli::cli app{
		"my-program",
		action{[](decli::arguments) -> int {
			return 0;
		}}
	};

	return app.execute(argc, argv);
}
```

Building and running our program gives us a ``--help`` flag already which echoes out some helpful text. We don't have any useful options yet. Let's add those now.

## Positional Arguments
Positional arguments are arguments without an associated ``--flag``. We create them with a ``decli::positional`` argument to our ``cli`` constructor:

```cpp
decli::cli app{
	// ...
	decli::positional{"input-file"},
	// ...
};
```

Our positional argument will appear in the **Usage** section of our ``--help`` output, like so:

	Usage:
	  bin/example <input-file>
	
	...

Positional arguments are required arguments by default, and the program will exit with an error and a help message if they are not provided. In the associated ``decli::arguments`` object generated, we can access the value of the positional argument using the subscript ``operator[]`` or the ``arg*``methods:

```cpp
// Gets the argument value. Since it's a required argument, we don't have to verify that it is present.
auto input_file = args["input-file"];

// Equivalent:
input_file = args.arg("input-file");
```

DeCLI doesn't (yet) support typed arguments, so its up to you to parse them as a specific type. For now, our argument types are just ``std::string`` objects.

We can require multiple positionals by simply adding more positional arguments to our program definition. As expected, they are parsed and stored in the order in which they are declared.

We can also create *optional* positional arguments. These are positional arguments that will only be present in ``decli::arguments`` if the user provided more arguments than are required, but few enough to fit in our optional arguments. To make a positional optional, pass ``required{false}`` to its constructor:

```cpp
decli::cli app{
	// ...
	decli::positional{"output-file", required{false}},
	// ...
};
```

These are also parsed in order, but are forcibly parsed *after* all required positionals, regardless of any interleaving in the program definition.

Since they are *optional*, they may not be present in the ``decli::arguments`` passed to our action. For this case, we has two methods:

```cpp
// Defaults to "a.out" if the user didn't provide an additional positional
auto output_file = args.arg_or("output-file", "a.out");

// Returns a pointer to a std::string, nullptr if not provided
auto maybe_output_file = args.maybe_arg("output-file");
if (maybe_output_file) { /* Do stuff */ }
else { /* Do other stuff */ }
```

## Parameters
Parameters are values with associated names. In most cases, these names are prefixed by two hyphens "``--``"

In DeCLI, unlike some other command lines, parameters are *always optional* with respect to the parser. An error may be raised by the ``action`` if one wishes to do so, but DeCLI doesn't assume them to be required.

Adding a parameter is just like adding a positional. We use the shortened name ``param``:

```cpp
decli::cli app{
	// ...
	decli::param{"--extra-file"},
	// ...
};
```

This generates a command line option ``--extra-file`` that takes a single string value. It supports two styles:

```bash
# Two seperate arguments:
my-program --extra-file things.txt

# One argument:
my-program --extra-file=things.txt
```

Additionally, the two following are equivalent:

```bash
# Empty string
my-program --extra-file ""

# Empty string
my-program --extra-file=
```

**But** the following is an **error**:

```bash
# Error: Flag "--extra-file" expects an argument
my-program --extra-file
```

Accessing the value of a parameter is the same as accessing the value of a positional:

```cpp
auto extra = args.maybe_arg("--extra-file");
```

A difference is that the ``param`` constructor allows us to set a ``key``, which is the name which we use to access it later on. So, if we pass ``key{"extra"}`` to the ``param`` constructor, we should access the value with ``args.maybe_arg("extra")``. The key value will also appear in the help text.

### Setting Help and Summaries
parameters may have an associated ``help`` and/or ``summary``. This is displayed in the help output. The ``summary`` should be a short description of the parameter and should not contain the name of the parameter, while ``help`` can be more detailed:

```cpp
decli::param{
	"--extra-file",
	summary{"Specify an extra file to process"},
	help{
		"You can use --extra-file when you have an extra "
		"file that needs to be processed separately. "
		"Lorem ipsum dolor sit amet, consectetur adipiscing "
		"elit. Sed cursus, tortor at feugiat blandit, felis "
		"felis porta felis, vel iaculis tortor ligula eu nibh. "
		"Vestibulum ante ipsum primis in faucibus."
	}
},
```

There is no need to provide line breaks for the help or summary text. DeCLI will automatically reflow the text when displaying it to fit within 80 columns.

### Specifying Short-Form Parameters
DeCLI supports parameters having single-character shortened forms using the ``shorter`` parameter:

```cpp
decli::param{
	"--extra-file",
	shorter{'E'},
	// ...
},
```

The shortened form is used with a single hyphen followed by the shortened key character, and supports two forms:

```bash
# Two arguments:
my-program -E stuff.txt

# One argument:
my-program -Estuff.txt
```

The two above samples are equivalent.

The value of the shortened form parameters are accessed using the same method as if they were provided using the long form.

## Toggle (Switch) Arguments
In DeCLI, *switch* type arguments are called *toggle* arguments (The keyword ``switch`` was taken, and I'd prefer not requiring all uses to say ``switch_`` just to hack around that). Switch arguments toggle behaviours in the program and represent boolean values. They do not take a value, as their presence *is* the value.

Creating toggles is the same as creating valued parameters, and the constructor accepts the same arguments, except for ``key``:

```cpp
decli::toggle{
	"--do-the-extra-thing",
	shorter{'D'},
	summary{"Do that thing"},
	help{
		"This enables us to do that extra thing. "
		"Lorem ipsum dolor sit amet, consectetur adipiscing "
		"elit. Sed cursus, tortor at feugiat blandit, felis "
		"felis porta felis, vel iaculis tortor ligula eu nibh. "
		"Vestibulum ante ipsum primis in faucibus."
	}
},
```

Getting the value of a toggle is simple. Access it with the ``given`` method of an ``arguments`` object. This method will return ``true`` if the specified toggle (switch) was provided on the command line, and ``false`` otherwise.

### Multi-Character Shortened Arguments
Any good program will recognize multiple short-form arguments being merged together at once. Because of the nature of switches, this is easy to do. For example, if we have multiple switches with short forms ``-a``, ``-b``, ``-Q``. ``-l``, ``-p``, and ``-I``, the user can provide any combination of them as one argument like so:

```bash
my-program -aQlpI
```

In the case that we have a ``param`` with a short-form flag, we still support merging of short forms, but argument parsing stops at the ``param`` short character. If the ``param`` character is the last in the sequence, The next argument is consumed as the value. If the ``param`` character is not the last, the remaining characters in the sequence will be consumed as the value, even if they could also represent another parameter or toggle:

```bash
# Assuming we still have the -E from above, the value of
# --extra-file will by lpI, and none of those respective toggles
# will be enabled. But the -a and -Q ones will be, since they
# preceed the `E` option:
my-program -aQElpI

# On the other hand, we can pass the value of -E in the next parameter
# by making it he last in the sequence. This is equivalent to the above
# example:
my-program -aQE lpI
```
